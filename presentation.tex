\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{lmodern}

\usetheme[compressed]{Berlin}
\usecolortheme{beaver}

\title{Introducing Wrek}

\subtitle{A Library For Executing Dependency Graphs}

\author{Richard Kallos}
\institute{Samsung Ads Canada (formerly Adgear)}
\date{Code BEAM SF, March 2018 \\
\url{https://gitlab.com/rkallos/code-beam-2018/raw/master/presentation.pdf}}

% Show Table of Contents at beginning of each section
\AtBeginSection[]{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}[c]
  \frametitle{What is wrek?}
  Wrek is a library for executing task dependency graphs.
\end{frame}

\begin{frame}
  \frametitle{What is wrek?}
  Given a graph like:

  \begin{overlayarea}{\textwidth}{0.4\textheight}
    \includegraphics<+>[height=0.4\textheight]{img/dag1-0.eps}
    \includegraphics<+>[height=0.4\textheight]{img/dag1-1.eps}
    \includegraphics<+>[height=0.4\textheight]{img/dag1-2.eps}
    \includegraphics<+>[height=0.4\textheight]{img/dag1-3.eps}
  \end{overlayarea}

  Wrek executes tasks in topological order
\end{frame}

\section{Theory}

\subsubsection{Parallelism}

\begin{frame}
  \frametitle{\subsubsecname}
  \uncover<+->{There are many kinds of parallelism in computing.}

  \uncover<+->{Two kinds of parallelism that are often mentioned together are:}

  \begin{itemize}[<+->]
  \item Data parallelism: Splitting data across processors
  \item Task parallelism: Splitting tasks across processors
  \end{itemize}

  \uncover<+->{These two forms can be (and are) used together!}

  \uncover<+->{e.g. Image processing consists of pipelines of data-parallel tasks}
\end{frame}

\subsubsection{Dependency Graphs}

\begin{frame}
  \frametitle{\subsubsecname}
  \begin{itemize}[<+->]
  \item A \alert{dependency graph} is a directed acyclic graph (DAG) whose edges model a dependency
    relation between vertices.

  \item An edge (a, b) in a dependency graph means \\``a depends on b''.

  \item An edge (b, a) in the transpose of the graph means \\ ``b is a dependency of a''

  \item Vertices with no paths connecting them can execute concurrently
  \end{itemize}
\end{frame}

\begin{frame}[c]
  \frametitle{\subsubsecname}
  \only<1>{Here is a dependency Graph...}
  \only<2>{... and its transpose}

  \begin{overlayarea}{\textwidth}{0.6\textheight}
    \centering
    \includegraphics<1>[height=0.6\textheight]{img/dag2-1.eps}
    \includegraphics<2>[height=0.6\textheight]{img/dag2-2.eps}
  \end{overlayarea}
\end{frame}

\begin{frame}
  \frametitle{\subsubsecname}
  In addition to being widespread in computing, dependency graphs are also used by humans!
  \pause
  \begin{itemize}[<+->]
  \item Many of the lists we make are topological orderings of dependency graphs \\
    e.g. to-do lists, cooking recipes, checklists \\
    e.g. 1. Foo the bar. 2. Baz the foo'd bar...
  \end{itemize}
\end{frame}

\begin{frame}[c]
  \frametitle{\subsubsecname}
  Cooking recipes are topological orderings of dependency graphs. \\
  \centering
  \includegraphics[height=0.6\textheight]{img/dag0.eps}
\end{frame}

\subsubsection{Topological ordering}

\begin{frame}[<+->]
  \frametitle{\subsubsecname}
  \begin{itemize}
  \item Topological ordering: For every edge (u, v), u comes before v.
  \item A topological ordering of a dependency graph is a valid evaluation order.
  \item e.g. [boil\_water, chop\_vegetables, add\_pasta, purée\_tomatoes, add\_spices, ...]
  \item Topo-sorting dependency graphs \emph{discards information} about possible concurrency
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{A Thought}
  \centering
  What if we could write arbitrary code as dependency graphs and have them execute with maximum concurrency?
\end{frame}

\section{Design of Wrek}

\begin{frame}
  \frametitle{\secname}
  \begin{itemize}[<+->]
  \item OTP behaviours let library/application developers separate the \emph{general} from the \emph{specific}
  \item General: Executing dependency graphs in proper order
  \item Specific: The structure of dependency graphs
  \item Specific: Executing single vertices
  \end{itemize}
\end{frame}

\subsubsection{General}

\begin{frame}[c]
  \frametitle{\subsubsecname}
  \centering
  \includegraphics[height=0.6\textheight]{img/dag3.eps}
\end{frame}

\begin{frame}[c]
  \frametitle{\subsubsecname}
  \centering
  \includegraphics[height=0.6\textheight]{img/dag4.eps}
\end{frame}

\subsubsection{Specific}

\begin{frame}[fragile]
  \frametitle{\subsubsecname}
  \begin{semiverbatim}
-module(wrek_vert).

-callback run(Args :: list(), Parent :: pid()) ->
    \{ok, Result :: any()\} | \{error, Reason :: any()\}.
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\subsubsecname}
  \begin{semiverbatim}
-type dag_map() :: #\{any() := vert_defn()\} |
                   [\{any(),   vert_defn()\}].

-type vert_defn() :: #\{
    module := module(),
    args   := list(),
    deps   := list()
\}.
  \end{semiverbatim}
\end{frame}

\begin{frame}[<+->]
  \frametitle{What happens when you call \texttt{wrek:start/2}}
  \begin{itemize}
  \item The supplied map is read into a \texttt{digraph:graph()}
  \item All vertices with no queued or running dependencies are \texttt{spawn\_link}ed
  \item Values returned from vertices are stored in labels for use by later vertices via \texttt{wrek\_vert:get/3}
  \item Calls \texttt{gen\_event:notify/2} with \texttt{\#wrek\_event\{\}} records to an optional \texttt{gen\_event} process
  \item If \texttt{wrek\_vert:Module:run/2} returns an error or throws an exception, the crash propagates to the rest of the graph.
  \end{itemize}
\end{frame}

\section{Use}

\subsubsection{Wrek @ \$WORK}

\begin{frame}[<+->]
  \frametitle{\subsubsecname}
  \begin{itemize}
  \item Our edge servers (bidders) were all wasting a CPU core doing the same calculation
  \item Solution: Do the calculation off the edge, and ship the result
  \item Version 1 was implemented with...\onslide<+->\texttt{cron} and \texttt{bash}
  \end{itemize}
\end{frame}

\begin{frame}[<+->]
  \frametitle{\subsubsecname}
  \begin{itemize}
  \item Version 1 mostly worked, but we realized this offered opportunities to take pressure off services on other servers
  \item If we are going to extend this new system, it would be better to create a more robust framework
  \item Enter Erlang!
  \end{itemize}
\end{frame}

\subsubsection{Erlang to the rescue!}

\begin{frame}[<+->]
  \frametitle{\subsubsecname}
  \begin{itemize}
  \item In order to iterate quickly, it made sense to have a library that could
    \begin{itemize}
    \item Run Erlang callbacks
    \item Run our already-existing shell scripts (secretly topological orderings of dependency graphs)
    \end{itemize}
  \item Wrek was the result
  \end{itemize}
\end{frame}

\begin{frame}[<+->]
  \frametitle{\subsubsecname}
  \begin{itemize}
  \item Wrek was able to easily slurp our existing scripts (thanks to \texttt{erlexec})
  \item This allowed for piecemeal replacement of large-ish scripts with dependency graphs of smaller scripts and Erlang callbacks
  \item This offered better concurrency and (much) more information for logging/monitoring
  \end{itemize}
\end{frame}

\begin{frame}[<+->]
  \frametitle{\subsubsecname}
  Thanks to Erlang/OTP, we are flexible to handle events generated by Wrek
  \onslide<+->
  \begin{itemize}
  \item Exposing status of executing graphs via a HTTP endpoint
  \item Establishing contracts between on- and off-edge servers
  \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}[<+->]
  \frametitle{\secname}
  \begin{itemize}
  \item Dependency graphs are useful for exposing opportunities for concurrency
  \item Dependency graphs show up all over the place, in computing and in everyday life
  \item Wrek is an application that executes dependency graphs
  \item Erlang/OTP has been instrumental in letting us build and ship quickly, and start paying down our shell-script technical debt
  \item Big thanks to \texttt{digraph} and \texttt{erlexec}; they do the heavy lifting.
  \end{itemize}
\end{frame}

\begin{frame}[<+->]
  \frametitle{\secname}
  If you
  \onslide<+->
  \begin{itemize}
  \item Enjoy thinking about larger tasks being composed of dependency graphs of smaller tasks (Try it! It's fun!)
  \item Want to incrementally replace a mess of shell scripts with smaller, more concurrent ones (or Erlang code)
  \end{itemize}
  \onslide<+->
  then you might enjoy Wrek!
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \begin{itemize}
  \item \url{http://github.com/rkallos/wrek}
  \item \url{http://github.com/saleyn/erlexec}
  \item \url{http://erlang.org/doc/man/digraph.html}
  \end{itemize}
\end{frame}

\begin{frame}[c]
  \frametitle{\secname}
  \center{\huge{Thank you!}}
\end{frame}

\end{document}
